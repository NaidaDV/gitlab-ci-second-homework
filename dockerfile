# Using Ubuntu 18.04
FROM ubuntu:18.04
# Information about me
MAINTAINER denys naida <NaidaDV@nmu.one>
# Updating and upgrading; installing java, wget and curl 
RUN apt-get update && apt-get upgrade -y && apt-get install openjdk-11-jdk -y && apt-get install wget -y && apt-get install curl -y
# Copying application .jar
COPY ./springbootwebapp/target/spring-boot-web-0.0.1-SNAPSHOT.jar /project/
EXPOSE 8080
# Starting application
ENTRYPOINT java -jar /project/spring-boot-web-0.0.1-SNAPSHOT.jar