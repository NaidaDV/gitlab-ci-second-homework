# Gitlab-CI second homework

## Table of contents 
1) App source 
2) gitlab-ci.yml - content with comments
3) Dockerfile - content with comments
4) Workflow
## App source
For this homework I choose the Java project, stored in https://github.com/springframeworkguru/springbootwebapp , that I have used in Docker homework. I downloaded it on my PC and uploaded it to this repo.
## gitlab-ci.yml - content with comments
``` 
image: docker:dind

# Starting docker in docker, adding to service alias - localhost for testing image
services:
  - name: docker:dind
    alias: localhost

variables:
  img_name: my-image
  container_name: my-container

stages:
  - compile
  - build
  - test

# Compiling application, transition artifacts to the next stage
Compile application:
  image: maven
  stage: compile
  script:
    mvn package -f './springbootwebapp/pom.xml'
  artifacts:
    paths:
      - ./springbootwebapp/target/spring-boot-web-0.0.1-SNAPSHOT.jar


# Building image with running application from dockerfile, saving image as artifact
Building image:
  stage: build
  script:
    - echo "Building image from Dockerfile"
    - >
      docker build
      --tag "${img_name}":"$CI_PIPELINE_ID"
      .
    - docker save "${img_name}":"$CI_PIPELINE_ID" > ${img_name}.tar
  artifacts:
    paths:
      - ${img_name}.tar

# Running container from created image, testing 
Testing image with web application:
  stage: test
  before_script:
    - apk add --update wget && rm -rf /var/cache/apk/*
  script:
    - echo "Testing web-app, running in container, with wget and curl"
    - docker load < ${img_name}.tar
    - docker run -itd -p 8081:8080 --name ${container_name} ${img_name}:"$CI_PIPELINE_ID"
    - docker exec -t ${container_name} wget -S localhost:8080
    - docker exec -t ${container_name} curl localhost:8080
```
## Dockerfile - content with comments
```
# Using Ubuntu 18.04
FROM ubuntu:18.04
# Information about me
MAINTAINER denys naida <NaidaDV@nmu.one>
# Updating and upgrading; installing java, wget and curl
RUN apt-get update && apt-get upgrade -y && apt-get install openjdk-11-jdk -y && apt-get install wget -y && apt-get install curl -y
# Copying application .jar
COPY ./springbootwebapp/target/spring-boot-web-0.0.1-SNAPSHOT.jar /project/
EXPOSE 8080
# Starting application
ENTRYPOINT java -jar /project/spring-boot-web-0.0.1-SNAPSHOT.jar
```
## Workflow
There are 3 scripted stages:
![](screenshots/1.png)

Here are the results of the application tests:

![](screenshots/2.png)

![](screenshots/3.png)

Also homework include runtime_output.txt where you can find full successful output from GitLab runtime.
